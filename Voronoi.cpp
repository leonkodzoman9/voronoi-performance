#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include <vector>
#include <random>
#include <chrono>
#include <string>
#include <execution>
#include <numeric>

#include <immintrin.h>

static constexpr int Width = 1600;
static constexpr int Height = 900;

static constexpr int PointCount = 32;

struct vec2 {
	float x = 0;
	float y = 0;
};

struct vec4 {
	float x = 0;
	float y = 0;
	float z = 0;
	float w = 0;
};



void voronoi(std::vector<vec4>& frame, const std::vector<vec2>& points, const std::vector<vec4>& colors) {

	for (int i = 0; i < Height; i++) {
		for (int j = 0; j < Width; j++) {

			int nearestIndex = 0;
			float nearestDistance = FLT_MAX;
			for (int p = 0; p < points.size(); p++) {

				float dx = points[p].x - j;
				float dy = points[p].y - i;
				float diag = std::sqrt(dx * dx + dy * dy);

				if (diag < nearestDistance) {
					nearestDistance = diag;
					nearestIndex = p;
				}
			}

			frame[i * Width + j] = colors[nearestIndex];
		}
	}
}

void voronoiOpt(std::vector<vec4>& frame, const std::vector<vec2>& points, const std::vector<vec4>& colors) {

}



void bench(std::vector<vec4>& frame, const std::vector<vec2>& points, const std::vector<vec4>& colors) {

	auto t1 = std::chrono::steady_clock::now();

	for (int i = 0; i < 10; i++) {
		voronoi(frame, points, colors);
	}

	auto t2 = std::chrono::steady_clock::now();

	for (int i = 0; i < 10; i++) {
		voronoiOpt(frame, points, colors);
	}

	auto t3 = std::chrono::steady_clock::now();

	float dt1 = (t2 - t1).count() / 1e6;
	float dt2 = (t3 - t2).count() / 1e6;

	printf("Base %f ms\n", dt1 / 10);
	printf("Optm %f ms\n", dt2 / 10);
	printf("Spdp %f times\n", dt1 / dt2);
}



int main() {

	glfwInit();
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 6);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_COMPAT_PROFILE);

	GLFWwindow* window = glfwCreateWindow(1600, 900, "Voronoi", nullptr, nullptr);
	glfwMakeContextCurrent(window);

	gladLoadGLLoader((GLADloadproc)glfwGetProcAddress);
	glfwSwapInterval(0);



	std::mt19937 gen(0);
	std::uniform_real_distribution<float> dist;

	std::vector<vec2> points;
	std::vector<vec2> velocities;
	std::vector<vec4> colors;
	for (int i = 0; i < PointCount; i++) {

		vec2 p;
		p.x = dist(gen) * (Width - 100.0f) + 50.0f;
		p.y = dist(gen) * (Height - 100.0f) + 50.0f;
		points.push_back(p);

		vec2 v;
		v.x = dist(gen) * 2.0f - 1.0f;
		v.y = dist(gen) * 2.0f - 1.0f;
		velocities.push_back(v);

		vec4 c;
		c.x = dist(gen);
		c.y = dist(gen);
		c.z = dist(gen);
		c.w = 1.0f;
		colors.push_back(c);
	}



	std::vector<vec4> frame(Width * Height);

	while (!glfwWindowShouldClose(window)) {

		auto t1 = std::chrono::steady_clock::now();

		glfwPollEvents();

		glClear(GL_COLOR_BUFFER_BIT);

		for (int i = 0; i < points.size(); i++) {
			points[i].x += velocities[i].x;
			points[i].y += velocities[i].y;
			if (points[i].x < 10.0f || points[i].x > Width - 10.0f) {
				velocities[i].x *= -1;
			}
			if (points[i].y < 10.0f || points[i].y > Height - 10.0f) {
				velocities[i].y *= -1;
			}
		}

		auto t2 = std::chrono::steady_clock::now();

		voronoi(frame, points, colors);

		auto t3 = std::chrono::steady_clock::now();

		glDrawPixels(Width, Height, GL_RGBA, GL_FLOAT, frame.data());

		glfwSwapBuffers(window);

		auto t4 = std::chrono::steady_clock::now();

		float msTotal = (t4 - t1).count() / 1e6;
		float msAlgorithm = (t3 - t2).count() / 1e6;

		static float f = 0;
		f += msTotal;
		if (f > 100.0f) {
			f -= 100.0f;
			std::string time = std::to_string(msAlgorithm) + " / " + std::to_string(msTotal);
			glfwSetWindowTitle(window, time.data());
		}

		if (glfwGetKey(window, GLFW_KEY_B)) {
			bench(frame, points, colors);
		}
	}

	return 0;
}